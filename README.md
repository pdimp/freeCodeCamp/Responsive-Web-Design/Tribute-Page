## Task

---
### Responsive Web Design Projects - Build a Tribute Page

**Objective**: Build a CodePen.io app that is functionally similar to [this](https://codepen.io/freeCodeCamp/full/zNqgVx).

Fulfill the below user stories and get all the tests to pass. Give it your own personal style.

You can use HTML, JavaScript, and CSS to complete this project. Plain CSS is recommended because that is what the lessons have covered so far and you should get some practice with plain CSS. You can use Bootstrap or SASS if you choose. Additional technologies (just for example jQuery, React, Angular, or Vue) are not recommended for this project, and using them is at your own risk. Other projects will give you a chance to work with different technology stacks like React. We will accept and try to fix all issue reports that use the suggested technology stack for this project. Happy coding!

**_User Story #1:_** My tribute page should have an element with a corresponding id="main", which contains all other elements.

**_User Story #2:_** I should see an element with a corresponding id="title", which contains a string (i.e. text) that describes the subject of the tribute page (e.g. "Dr. Norman Borlaug").

**_User Story #3:_** I should see a div element with a corresponding id="img-div".

**_User Story #4:_** Within the img-div element, I should see an img element with a corresponding id="image".

**_User Story #5:_** Within the img-div element, I should see an element with a corresponding id="img-caption" that contains textual content describing the image shown in img-div.

**_User Story #6:_** I should see an element with a corresponding id="tribute-info", which contains textual content describing the subject of the tribute page.

**_User Story #7:_** I should see an a element with a corresponding id="tribute-link", which links to an outside site that contains additional information about the subject of the tribute page. HINT: You must give your element an attribute of target and set it to _blank in order for your link to open in a new tab (i.e. target="_blank").

**_User Story #8:_** The img element should responsively resize, relative to the width of its parent element, without exceeding its original size.

**_User Story #9:_** The img element should be centered within its parent element.

You can build your project by forking [this](https://codepen.io/freeCodeCamp/full/zNqgVx) CodePen pen. Or you can use [this CDN link](https://cdn.freecodecamp.org/testable-projects-fcc/v1/bundle.js) to run the tests in any environment you like.

Once you're done, submit the URL to your working project with all its tests passing.

---

## Solution

---

### Link
[pdimp / freeCodeCamp / Responsive Web Design / Tribute Page](https://pdimp.gitlab.io/freeCodeCamp/Responsive-Web-Design/Tribute-Page)

---

### Screenshots

![](screenshots/1.jpg)
![](screenshots/2.jpg)